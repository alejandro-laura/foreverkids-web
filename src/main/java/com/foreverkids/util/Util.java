package com.foreverkids.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Util {
	
	public static boolean validarFormato(String fecha, String formato) {
		try {
			LocalDate.parse(fecha, DateTimeFormatter.ofPattern(formato));
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}

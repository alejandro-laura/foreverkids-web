package com.foreverkids;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ForeverkidsWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(ForeverkidsWebApplication.class, args);
	}

}

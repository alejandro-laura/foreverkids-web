package com.foreverkids.controller;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.foreverkids.controller.dto.SessionDto;

import com.foreverkids.controller.validator.PasswordDtoValidator;
import com.foreverkids.model.Usuario;
import com.foreverkids.service.IUsuarioService;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.foreverkids.controller.dto.PasswordDto;

@Controller
@RequestMapping("/user")
public class PasswordController {
	@Autowired
	private IUsuarioService usuarioService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
	private SessionDto session;


    	
	@GetMapping("/password")
	public String changePassword(Model model) {
	
			//si ya esta logueado se redigira a /

           model.addAttribute("password", new PasswordDto());
			return "usuarios/ChangePassword";

	}


    //codigo nuevo agregado
	@PostMapping("/password")
    public String processChangePassword(HttpServletRequest request, Model model,
                                        @Valid @ModelAttribute("password")PasswordDto dto, BindingResult result,
                                        RedirectAttributes ra
                                       )
                                        throws ServletException {

        String oldPassword = request.getParameter("otrClaUsu");
        String newPassword = request.getParameter("otrNuevaClaUsu");
   



        Usuario user = usuarioService.listarporDni(session.getOtrDniUsu());
   
       
         new PasswordDtoValidator(user,0).validate(dto, result);
           if (result.hasErrors()) { 

               return "usuarios/changePassword";
           }
      


           if (!passwordEncoder.matches(oldPassword, user.getOtrClaUsu())) {

            new PasswordDtoValidator(user,1).validate(dto, result);
     
            if (result.hasErrors()) { 

                return "usuarios/changePassword";
            }
           }

  
            
            usuarioService.changePassword(user, newPassword);

            request.logout();
            ra.addFlashAttribute("message", "Su nuevo password ha sido actualizado exitosamente! "
                    + "Por favor, loguearse nuevamente.");
            
     

            return "redirect:/usuarios/listar";
            
            
        }
 
    }
    




package com.foreverkids.controller;

import java.time.format.DateTimeFormatter;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.foreverkids.controller.dto.EditarDto;
import com.foreverkids.controller.dto.UsuarioDto;
import com.foreverkids.controller.validator.UsuarioDtoValidator;
import com.foreverkids.model.Usuario;
import com.foreverkids.service.ITipoUsuarioService;
import com.foreverkids.service.IUsuarioService;
import com.foreverkids.util.Constantes;
import org.springframework.validation.BindingResult;

@Controller
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	private IUsuarioService usuarioService;
	@Autowired
	private ITipoUsuarioService tipoUsuarioService;
	
	@GetMapping(value = {"/", "/listar","/listar/{pageNo}"})
	public String paginacion(@PathVariable (name = "pageNo", required = false) Integer pageNo, 
			@RequestParam(name = "sortField", required = false, defaultValue = "id") String sortField,
			@RequestParam(name = "sortDir", required = false, defaultValue = "asc") String sortDir, Model model) {
		
		int pageSize = Constantes.PAGE_SIZE;
		pageNo = pageNo == null ? 1 : pageNo;
		Page<Usuario> page = usuarioService.paginacion("", "", "", pageNo, pageSize, sortField, sortDir);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		model.addAttribute("usuarios", page.getContent());
		model.addAttribute("editarDto", new EditarDto());
		return "usuarios/listar";
	}

	@PostMapping("/registrar")
	public String irNuevo(Model model) {
		model.addAttribute("accion", "Registrar");
		model.addAttribute("usuario", new UsuarioDto());
		model.addAttribute("tipoUsuarios", tipoUsuarioService.findAll());
		return "usuarios/form";
	}
	
	@PostMapping("/editar")
	public String irEditar(EditarDto dto, Model model) {
		Usuario usuario = usuarioService.listarPorId(new Long(dto.getCodigo())).get();
		UsuarioDto usuarioDto = new UsuarioDto(usuario.getId().toString(),
				usuario.getOtrDniUsu(), usuario.getDesApeUsu(), 
				usuario.getDesNomUsu(), "", 
				usuario.getTipoUsuario().getId().toString(), "");
		usuarioDto.setFecNacUsu(usuario.getFecNacUsu().format(DateTimeFormatter.ofPattern(Constantes.YYYY_MM_DD)));
		
		model.addAttribute("accion", "Editar");
		model.addAttribute("usuario", usuarioDto);
		model.addAttribute("tipoUsuarios", tipoUsuarioService.findAll());
		return "usuarios/form";
	}
	
	@PostMapping("/operar")
	public String operar(@Valid @ModelAttribute("usuario")UsuarioDto dto, BindingResult result, 
			RedirectAttributes redirAttrs, Model model) {
		
		String operacion = dto.getId() == null ? Constantes.REGISTRAR : Constantes.EDITAR;
		new UsuarioDtoValidator(usuarioService, operacion).validate(dto, result);
		
		if (result.hasErrors()) {
			model.addAttribute("accion", "Registrar");
			model.addAttribute("tipoUsuarios", tipoUsuarioService.findAll());
			return "usuarios/form";
		}
		
		if(dto.getId() == null) {
			usuarioService.registrar(dto);
		}else {
			usuarioService.editar(dto);
		}
		
		String operacionMsg = dto.getId() == null ? "registro" : "edito";
		redirAttrs.addFlashAttribute("message", String.format("Se %s correctamente el usuario.", (operacionMsg)));
		return "redirect:/usuarios/listar";
	}
	
	@PostMapping("/eliminar")
	public String delete(EditarDto dto, RedirectAttributes redirAttrs) {
		Long id = new Long(dto.getCodigo());
		//Long id = Long.parseLong(dto.getCodigo());
		usuarioService.eliminar(id);
		redirAttrs.addFlashAttribute("message", "Se elimino correctamente el usuario.");
		return "redirect:/usuarios/listar";
	}



}

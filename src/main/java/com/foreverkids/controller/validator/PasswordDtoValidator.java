package com.foreverkids.controller.validator;


import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.apache.commons.lang3.StringUtils;

import com.foreverkids.controller.dto.PasswordDto;
import com.foreverkids.model.Usuario;



public class PasswordDtoValidator implements Validator {


 
    private int status;


    public PasswordDtoValidator(Usuario user,int status) {

      
         this.status= status;
	} 
 

    @Override
    public boolean supports(Class<?> clazz) {
        return PasswordDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PasswordDto obj = (PasswordDto) target;
        if (status==0){
            
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "otrClaUsu", "otrClaUsu.required", "Ingrese la clave actual");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "otrNuevaClaUsu", "otrNuevaClaUsu.required", "Ingrese la nueva clave");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "otrRepNuevaClaUsu", "otrRepNuevaClaUsu.required", "Ingrese confirmación de la nueva clave");
           
    
         
          if(!StringUtils.isBlank(obj.getOtrClaUsu())   && !StringUtils.isBlank(obj.getOtrNuevaClaUsu())  && !StringUtils.isBlank(obj.getOtrRepNuevaClaUsu()))   {

    
    
       
            if(obj.getOtrClaUsu().equals(obj.getOtrNuevaClaUsu()))
            {
                errors.rejectValue("otrNuevaClaUsu", "{NotBlank.PasswordDto.otrNuevaClaUsu}", "La nueva contraseña debe ser diferente de la actual contraseña.");
            }
  
    
       
            if (!(obj.getOtrNuevaClaUsu().equals(obj.getOtrRepNuevaClaUsu())))
            {
                errors.rejectValue("otrRepNuevaClaUsu", "{NotBlank.PasswordDto.otrRepNuevaClaUsu}", "El campo debe coincidir con la nueva contraseña");
            } 
  
        }

    }
    else{
        errors.rejectValue("otrClaUsu", "{NotBlank.PasswordDto.otrClaUsu}", "La actual contraseña es incorrecta");
    }
}
 

       
	

    }
    

package com.foreverkids.controller.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.foreverkids.controller.dto.UsuarioDto;

import com.foreverkids.service.IUsuarioService;
import com.foreverkids.util.Constantes;
import com.foreverkids.util.Util;

public class UsuarioDtoValidator implements Validator {

	private IUsuarioService usuarioService;
	private String operacion;
	
	public UsuarioDtoValidator(IUsuarioService usuarioService, String operacion) {
		this.usuarioService = usuarioService;
		this.operacion = operacion;
	} 


	
	@Override
	public boolean supports(Class<?> clazz) {
		return UsuarioDto.class.isAssignableFrom(clazz);
	}



	@Override
	public void validate(Object target, Errors errors) {
		UsuarioDto obj = (UsuarioDto) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "otrDniUsu", "apePaterno.required", "Ingrese el documento de identidad.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "desApeUsu", "apePaterno.required", "Ingrese los apellidos.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "desNomUsu", "apePaterno.required", "Ingrese los nombres.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "idTipUsu", "apePaterno.required", "Ingrese el tipo de usuario.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fecNacUsu", "apePaterno.required", "Ingrese la fecha de nacimiento.");
		if(!StringUtils.isBlank(obj.getOtrDniUsu())) {
			if(StringUtils.length(obj.getOtrDniUsu()) != 8) {
				errors.rejectValue("otrDniUsu", "{NotBlank.usuarioDto.otrDniUsu}", "El dni debe tener 8 digitos.");
			}
			if(this.operacion.equals(Constantes.REGISTRAR)) {
				if(usuarioService.existsByOtrDniUsu(obj.getOtrDniUsu())) {
					errors.rejectValue("otrDniUsu", "{NotBlank.usuarioDto.otrDniUsu}", "El dni ya ha sido registrado.");
				}				
			}
		}	
		if(!StringUtils.isBlank(obj.getDesApeUsu())) {
			if(StringUtils.length(obj.getDesApeUsu()) > 40) {
				errors.rejectValue("desApeUsu", "{NotBlank.usuarioDto.otrDniUsu}", "Los apellidos no deben tener mas de 40 caracteres.");
			}
		}
		if(!StringUtils.isBlank(obj.getDesNomUsu())) {
			if(StringUtils.length(obj.getDesNomUsu()) > 40) {
				errors.rejectValue("desNomUsu", "{NotBlank.usuarioDto.otrDniUsu}", "Los nombres no deben tener mas de 40 caracteres.");
			}
		}
		if (!StringUtils.isBlank(obj.getFecNacUsu())) {
			if (!Util.validarFormato(obj.getFecNacUsu(), Constantes.YYYY_MM_DD)) {
				errors.rejectValue("fecNacUsu", "{NotBlank.usuarioDto.otrDniUsu}",
						String.format("La fecha debe tener el siguiente formato valido %s", Constantes.YYYY_MM_DD));
			}
		}
	}





}
	  



package com.foreverkids.controller;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class LoginController {


	@GetMapping(value = { "/login" })
	public String login(Model model, Principal principal) {
		if (principal != null) {
			//si ya esta logueado se redigira a /
			return "redirect:/";
		}
		return "login_3";
	}

	@GetMapping(value = { "/login1" })
	public String login1(Model model, Principal principal) {
		if (principal != null) {
			//si ya esta logueado se redigira a /
			return "redirect:/";
		}
		return "login_2";
	}

	@GetMapping("/")
	public String home(Model model) {
//		return "home";
		return "redirect:/usuarios/listar";
	}
}

package com.foreverkids.controller.dto;

public class SessionDto {

	private String desNomUsu;
	private String otrDniUsu;

	public String getDesNomUsu() {
		return desNomUsu;
	}

	public void setDesNomUsu(String desNomUsu) {
		this.desNomUsu = desNomUsu;
	}

	public String getOtrDniUsu() {
		return otrDniUsu;
	}

	public void setOtrDniUsu(String otrDniUsu) {
		this.otrDniUsu = otrDniUsu;
	}


}

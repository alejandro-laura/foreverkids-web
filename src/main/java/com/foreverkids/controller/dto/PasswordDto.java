package com.foreverkids.controller.dto;

public class PasswordDto {


    private Long id;
	private String otrClaUsu;
    private String otrNuevaClaUsu;
    private String otrRepNuevaClaUsu;


    public PasswordDto(){

    }

    public PasswordDto(Long id){
        this.id=id;
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getOtrClaUsu() {
        return otrClaUsu;
    }
    public void setOtrClaUsu(String otrClaUsu) {
        this.otrClaUsu = otrClaUsu;
    }
    public String getOtrNuevaClaUsu() {
        return otrNuevaClaUsu;
    }
    public void setOtrNuevaClaUsu(String otrNuevaClaUsu) {
        this.otrNuevaClaUsu = otrNuevaClaUsu;
    }
    public String getOtrRepNuevaClaUsu() {
        return otrRepNuevaClaUsu;
    }
    public void setOtrRepNuevaClaUsu(String otrRepNuevaClaUsu) {
        this.otrRepNuevaClaUsu = otrRepNuevaClaUsu;
    }

    

}
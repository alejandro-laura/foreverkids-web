package com.foreverkids.controller.dto;

import javax.validation.constraints.NotBlank;

public class UsuarioDto {

	private String id;
	@NotBlank
	private String otrDniUsu;
	private String desApeUsu;
	private String desNomUsu;
	private String otrClaUsu;
	private String idTipUsu;
	private String fecNacUsu;

	
	public UsuarioDto() {
		super();
	}
	
	public UsuarioDto(String id, String otrDniUsu, String desApeUsu, String desNomUsu, String otrClaUsu, String idTipUsu,
			String fecNacUsu) {
		super();
		this.id = id;
		this.otrDniUsu = otrDniUsu;
		this.desApeUsu = desApeUsu;
		this.desNomUsu = desNomUsu;
		
		this.otrClaUsu = otrClaUsu;
		this.idTipUsu = idTipUsu;
		this.fecNacUsu = fecNacUsu;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOtrDniUsu() {
		return otrDniUsu;
	}

	public void setOtrDniUsu(String otrDniUsu) {
		this.otrDniUsu = otrDniUsu;
	}

	public String getDesApeUsu() {
		return desApeUsu;
	}

	public void setDesApeUsu(String desApeUsu) {
		this.desApeUsu = desApeUsu;
	}

	public String getDesNomUsu() {
		return desNomUsu;
	}

	public void setDesNomUsu(String desNomUsu) {
		this.desNomUsu = desNomUsu;
	}

	public String getOtrClaUsu() {
		return otrClaUsu;
	}

	public void setOtrClaUsu(String otrClaUsu) {
		this.otrClaUsu = otrClaUsu;
	}

	public String getIdTipUsu() {
		return idTipUsu;
	}

	public void setIdTipUsu(String idTipUsu) {
		this.idTipUsu = idTipUsu;
	}

	public String getFecNacUsu() {
		return fecNacUsu;
	}

	public void setFecNacUsu(String fecNacUsu) {
		this.fecNacUsu = fecNacUsu;
	}

}

package com.foreverkids.controller.dto;





import java.util.Collection;

import com.foreverkids.model.Usuario;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UsuarioDetailDto implements UserDetails {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Usuario user;

    public UsuarioDetailDto(Usuario user){
        this.user=user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getPassword() {
        
        return user.getOtrDniUsu();
    }

    @Override
    public String getUsername() {
       
        return user.getOtrDniUsu();
    }

    @Override
    public boolean isAccountNonExpired() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isEnabled() {
        // TODO Auto-generated method stub
        return false;
    }


    public Usuario getUsuario(){
        return this.user;

    }

    
}

package com.foreverkids.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//@Entity
//@Table(name = "user", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
@Entity
@Table(name = "maetipusu")
public class TipoUsuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idtipusu")
	private Long id;

	@Column(name = "destipusu")
	private String desTipUsu;

	public TipoUsuario() {
		super();
	}

	public TipoUsuario(Long id) {
		super();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDesTipUsu() {
		return desTipUsu;
	}

	public void setDesTipUsu(String desTipUsu) {
		this.desTipUsu = desTipUsu;
	}

}

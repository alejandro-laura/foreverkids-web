package com.foreverkids.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "maeusu")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idusu")
	private Long id;

	@Column(name = "otrdniusu")
	private String otrDniUsu;

	@Column(name = "desapeusu")
	private String desApeUsu;

	@Column(name = "desnomusu")
	private String desNomUsu;

	@Column(name = "otrclausu")
	private String otrClaUsu;

	@ManyToOne
	@JoinColumn(name = "idtipusu", nullable = false)
	private TipoUsuario tipoUsuario;

	@Column(name = "fecnacusu")
	private LocalDate fecNacUsu;

	@Column(name = "otrurlzoo")
	private String otrUrlZoo;

	public Usuario() {
		super();
	}

	public Usuario(String otrDniUsu, String desApeUsu, String desNomUsu, String otrClaUsu) {
		super();
		this.otrDniUsu = otrDniUsu;
		this.desApeUsu = desApeUsu;
		this.desNomUsu = desNomUsu;
		this.otrClaUsu = otrClaUsu;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOtrDniUsu() {
		return otrDniUsu;
	}

	public void setOtrDniUsu(String otrDniUsu) {
		this.otrDniUsu = otrDniUsu;
	}

	public String getDesApeUsu() {
		return desApeUsu;
	}

	public void setDesApeUsu(String desApeUsu) {
		this.desApeUsu = desApeUsu;
	}

	public String getDesNomUsu() {
		return desNomUsu;
	}

	public void setDesNomUsu(String desNomUsu) {
		this.desNomUsu = desNomUsu;
	}

	public String getOtrClaUsu() {
		return otrClaUsu;
	}

	public void setOtrClaUsu(String otrClaUsu) {
		this.otrClaUsu = otrClaUsu;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getOtrUrlZoo() {
		return otrUrlZoo;
	}

	public void setOtrUrlZoo(String otrUrlZoo) {
		this.otrUrlZoo = otrUrlZoo;
	}

	public LocalDate getFecNacUsu() {
		return fecNacUsu;
	}

	public void setFecNacUsu(LocalDate fecNacUsu) {
		this.fecNacUsu = fecNacUsu;
	}

}

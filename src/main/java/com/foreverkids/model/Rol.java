package com.foreverkids.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//@Entity
//@Table(name = "user", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
@Entity
@Table(name = "maerol")
public class Rol {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idrol")
	private Long id;

	@Column(name = "desrol")
	private String desRol;

	public Rol() {
		super();
	}

	public Rol(String desRol) {
		super();
		this.desRol = desRol;
	}
		
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDesRol() {
		return desRol;
	}

	public void setDesRol(String desRol) {
		this.desRol = desRol;
	}

}

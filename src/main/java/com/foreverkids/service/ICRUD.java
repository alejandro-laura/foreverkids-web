package com.foreverkids.service;

import java.util.List;
import java.util.Optional;

public interface ICRUD<T, ID> {

	T save(T obj);
	List<T> findAll();
//	PageSupport<T> listarPage(Pageable page);
	Optional<T> findById(ID id);
	void deleteById(ID id);
}

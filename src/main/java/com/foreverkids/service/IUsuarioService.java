package com.foreverkids.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.foreverkids.controller.dto.UsuarioDto;
import com.foreverkids.model.Usuario;
import com.foreverkids.repository.IUsuarioRepository;

public interface IUsuarioService extends UserDetailsService {

	Usuario registrar(UsuarioDto dto);
	Usuario editar(UsuarioDto dto);
	List<Usuario> listar();
	Optional<Usuario> listarPorId(Long id);
	void eliminar(Long id);
	boolean existsByOtrDniUsu(String otrdniusu);
	Page<Usuario> paginacion(
			String otrDniUsu,
			String desNomUsu,
			String desApeUsu,
			int pageNo, int pageSize, String sortField, String sortDirection);
	//cambio agregado - Vanesa 	
	  void changePassword(Usuario user, String newPassword);

	//codigo agregado  29_03_2021
	Usuario listarporDni(String otrdniusu);
	  

}

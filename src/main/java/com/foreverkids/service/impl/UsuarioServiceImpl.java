package com.foreverkids.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.foreverkids.controller.dto.SessionDto;
import com.foreverkids.controller.dto.UsuarioDto;
import com.foreverkids.model.TipoUsuario;
import com.foreverkids.model.Usuario;
import com.foreverkids.repository.RolUsuarioRepository;
import com.foreverkids.repository.IUsuarioRepository;
import com.foreverkids.service.IUsuarioService;
import com.foreverkids.util.Constantes;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
@Service
public class UsuarioServiceImpl implements IUsuarioService {

	@Autowired
	private IUsuarioRepository userRepository;
	@Autowired
	private RolUsuarioRepository rolUsuarioRepository;
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	@Autowired
	private SessionDto session;

	@Override
	public UserDetails loadUserByUsername(String dni) throws UsernameNotFoundException {
		Usuario user = userRepository.findByOtrDniUsu(dni);
		if (user == null) {
			throw new UsernameNotFoundException("usuario o contraseña inválidos.");
		}
		if (session != null) {
			session.setDesNomUsu(String.format("%s %s", user.getDesNomUsu(), user.getDesApeUsu()));
			
          //codigo agregado
		  session.setOtrDniUsu(dni);

		}
		List<GrantedAuthority> authorities = new ArrayList<>();
		
		authorities.add(new SimpleGrantedAuthority("ADMIN"));
		return new org.springframework.security.core.userdetails.User(user.getOtrDniUsu(), user.getOtrClaUsu(),
				authorities);
	}
	
	@Transactional
	@Override
	public Usuario registrar(UsuarioDto dto) {
		Usuario usu = new Usuario();
		usu.setOtrDniUsu(dto.getOtrDniUsu());
		usu.setDesNomUsu(dto.getDesNomUsu());
		usu.setDesApeUsu(dto.getDesApeUsu());
		if (StringUtils.isBlank(dto.getOtrClaUsu())) {
			usu.setOtrClaUsu(passwordEncoder.encode(dto.getOtrDniUsu()));	
		} else {
			usu.setOtrClaUsu(passwordEncoder.encode(dto.getOtrClaUsu()));
		}
		usu.setTipoUsuario(new TipoUsuario(new Long(dto.getIdTipUsu())));
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constantes.YYYY_MM_DD);
		usu.setFecNacUsu(LocalDate.parse(dto.getFecNacUsu(), formatter));
//		System.out.println(userRepository.existsByOtrDniUsu(dto.getOtrDniUsu()));
		Usuario usuario = userRepository.save(usu);
		rolUsuarioRepository.registrar( Long.parseLong("1"), usuario.getId());
		return usuario;
	}

	@Override
	public Usuario editar(UsuarioDto dto) {
		Usuario obj = userRepository.findById(new Long(dto.getId())).get();
		obj.setDesNomUsu(dto.getDesNomUsu());
		obj.setDesApeUsu(dto.getDesApeUsu());
		if (!StringUtils.isBlank(dto.getOtrClaUsu())) {
			obj.setOtrClaUsu(passwordEncoder.encode(dto.getOtrClaUsu()));
		}
		obj.setTipoUsuario(new TipoUsuario(new Long(dto.getIdTipUsu())));
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Constantes.YYYY_MM_DD);
		
		obj.setFecNacUsu(LocalDate.parse(dto.getFecNacUsu(), formatter));
		return userRepository.save(obj);
	}

	@Override
	public List<Usuario> listar() {
		return userRepository.findAll();
	}

	@Override
	public Optional<Usuario> listarPorId(Long id) {
		return userRepository.findById(id);
	}



	
	@Transactional
	@Override
	public void eliminar(Long id) {
		rolUsuarioRepository.eliminar(id);
		userRepository.deleteById(id);
	}

	@Override
	public boolean existsByOtrDniUsu(String otrdniusu) {
		return userRepository.existsByOtrDniUsu(otrdniusu);
	}

	@Override
	public Page<Usuario> paginacion(String otrDniUsu, String desNomUsu, String desApeUsu, 
			int pageNo, int pageSize, String sortField, String sortDirection) {
		sortField = "id";
		sortDirection = "desc";
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
			Sort.by(sortField).descending();
		System.out.println("creando el pageable");
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
		System.out.println("pageable creado");
		return userRepository.findByOtrDniUsuContainingOrDesNomUsuContainingOrDesApeUsuContainingAllIgnoreCase(
				otrDniUsu, desNomUsu, desApeUsu, pageable);
	}

	@Override
	public void changePassword(Usuario user, String newPassword) {
		String encodedPassword = passwordEncoder.encode(newPassword);
		user.setOtrClaUsu(encodedPassword);
		userRepository.save(user);

         
   
		
	}



	@Override
	public Usuario listarporDni(String otrdniusu) {
		return userRepository.findByOtrDniUsu(otrdniusu);
	}
		
}
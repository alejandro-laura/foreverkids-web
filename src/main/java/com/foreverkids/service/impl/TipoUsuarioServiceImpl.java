package com.foreverkids.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foreverkids.model.TipoUsuario;
import com.foreverkids.repository.IGenericRepo;
import com.foreverkids.repository.ITipoUsuarioRepository;
import com.foreverkids.service.ITipoUsuarioService;

@Service
public class TipoUsuarioServiceImpl extends CRUDImpl<TipoUsuario, Long> implements ITipoUsuarioService{

	@Autowired
	private ITipoUsuarioRepository repo;
	
	@Override
	protected IGenericRepo<TipoUsuario, Long> getRepo(){
		return repo;
	}
	

}

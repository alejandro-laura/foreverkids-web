package com.foreverkids.service.impl;

import java.util.List;
import java.util.Optional;

import com.foreverkids.repository.IGenericRepo;
import com.foreverkids.service.ICRUD;

public abstract class CRUDImpl<T, ID> implements ICRUD<T, ID> {

	protected abstract IGenericRepo<T, ID> getRepo();

	@Override
	public T save(T obj) {
		return getRepo().save(obj);
	}

	@Override
	public List<T> findAll() {
		return getRepo().findAll();
	}

//	public Mono<PageSupport<T>> listarPage(Pageable page) {
//		////db.platos.find().skip(5).limit(5)
//		return getRepo().findAll()
//				.collectList()
//				.map(list -> new PageSupport<>(
//						list
//						.stream()
//						.skip(page.getPageNumber() * page.getPageSize())
//						.limit(page.getPageSize())
//						.collect(Collectors.toList()),
//					page.getPageNumber(), page.getPageSize(), list.size()						
//					));
//	}

	@Override
	public Optional<T> findById(ID id) {
		return getRepo().findById(id);
	}

	@Override
	public void deleteById(ID id) {
		getRepo().deleteById(id);
	}

}

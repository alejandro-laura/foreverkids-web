package com.foreverkids.service;

import com.foreverkids.model.TipoUsuario;

public interface ITipoUsuarioService extends ICRUD<TipoUsuario, Long> {

}

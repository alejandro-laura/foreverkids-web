package com.foreverkids.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foreverkids.model.Usuario;

@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario, Long> {
	Usuario findByOtrDniUsu(String otrdniusu);
	boolean existsByOtrDniUsu(String otrdniusu);
	Page<Usuario> findByOtrDniUsuContainingOrDesNomUsuContainingOrDesApeUsuContainingAllIgnoreCase(
			String otrDniUsu,
			String desNomUsu,
			String desApeUsu,
			Pageable pageable);


}

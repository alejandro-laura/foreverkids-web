package com.foreverkids.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.foreverkids.model.RolUsuario;

@Repository
public interface RolUsuarioRepository extends JpaRepository<RolUsuario, Integer> {

	// @Transactional
	@Modifying
	@Query(value = "INSERT INTO relrolusu(idrol, idusu) VALUES (:idRol, :idUsu)", nativeQuery = true)
	int registrar(@Param("idRol") Long idRol, @Param("idUsu") Long idUsu);
	
	@Modifying
	@Query(value = "DELETE FROM relrolusu WHERE idusu=:idUsu", nativeQuery = true)
	int eliminar(@Param("idUsu") Long idUsu);
	
}

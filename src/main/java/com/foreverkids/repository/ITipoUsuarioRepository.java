package com.foreverkids.repository;

import com.foreverkids.model.TipoUsuario;

public interface ITipoUsuarioRepository extends IGenericRepo<TipoUsuario, Long> {

}
